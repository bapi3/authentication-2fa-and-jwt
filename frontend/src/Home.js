 //Home.js
 import React from 'react'
 import TopBar from './TopBar'

 const Home = () => {
   return (
     <div>
         <TopBar />
         <h1>Home</h1>
     </div>
   )
 }

 export default Home