import logo from './logo.svg';
// import './App.css';
import { Route, Routes } from 'react-router-dom';
import Register from './Register';
import Home from './Home';
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Routes>
         <Route path='/' element={<Home/>} />
         <Route path='/register' element={<Register/>} />
        </Routes>
      </header>
    </div>
  );
}

export default App;
