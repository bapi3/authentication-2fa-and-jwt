import React, { useState } from 'react';
import axios from 'axios';
import { TextField, Radio } from '@mui/material';
import QRCode from 'qrcode.react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import TopBar from './TopBar'
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/esm/Button';
import Card from 'react-bootstrap/Card';
const Register = () => {
  const [formData, setFormData] = useState({
    firstname: '',
    lastname: '',
    email: '',
    password: '',
    mfaEnabled: false,

  });

  const [qrCode, setQRCode] = useState('');
  const [otp, setOTP] = useState('');
  const [step, setStep] = useState(1);

  const handleChange = (e) => {
    let { name, value } = e.target;
    if(name == "mfaEnabled" && e.target.checked){
      value = true
    }
    if(name == "mfaEnabled" && !e.target.checked){
      value = false;
    }
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));

  };

  const handleRegister = async () => {
    try {
      const response = await axios.post('http://localhost:8081/api/v1/auth/register', formData);
      if (response.data.status) {
        if(response.data.mfaEnabled){
          setQRCode(response.data.secretImageUri);
          setStep(2);
        }else{
          setStep(3);
        }

      } else {
        toast.error(response.data.message);
      }
    } catch (error) {
      console.error(error);
      toast.error('An error occurred while registering.');
    }
  };

  const handleVerifyOTP = async () => {
    try {
      const response = await axios.post('YOUR_BACKEND_API_URL/verifyOTP', { otp });
      if (response.data.success) {
        toast.success('Registration successful!');
        setStep(3);
      } else {
        toast.error(response.data.message);
      }
    } catch (error) {
      console.error(error);
      toast.error('An error occurred while verifying OTP.');
    }
  };

  return (
    <div>
        <TopBar/>
      <Container className='col-md-5 mt-2'>
      <ToastContainer />
      <Card>
        <Card.Header>Registration Form</Card.Header>
        <Card.Body>
      {step === 1 && (
          <Form>
            <Form.Group className="mb-3" controlId="firstname">
              <Form.Label>First Name</Form.Label>
              <Form.Control type="text" placeholder="firstName" name="firstname" value={formData.firstname} onChange={handleChange} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="lasttname">
              <Form.Label>Last Name</Form.Label>
              <Form.Control type="text" placeholder="lastName" name="lastname" value={formData.lastname} onChange={handleChange}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" placeholder="email" name="email" value={formData.email} onChange={handleChange} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="password" name="password" value={formData.password} onChange={handleChange}/>              
            </Form.Group>
            <Form.Group className="mb-3" controlId="mfaEnabled">
              <Form.Check // prettier-ignore
                type="switch"
                name="mfaEnabled"
                label="mfaEnabled"
                onChange={handleChange}
                defaultChecked={formData.mfaEnabled}
              />
            </Form.Group>
            <Form.Group className="mb-3 d-flex justify-content-center" controlId="register_button">
              <Button variant="primary" onClick={handleRegister}>Submit</Button>
            </Form.Group>
          </Form>
      )}
      {step === 2 && (
        <div>
          <Form.Group className="mb-3 text-center" controlId="qr">
            <img src={qrCode} alt="QR Code" width="160px"/>
          </Form.Group>
          <Form.Group className="mb-3" controlId="otp">
            <Form.Label>Enter OTP</Form.Label>
            <Form.Control type="number" placeholder="OTP" name="otp" value={otp} onChange={(e) => setOTP(e.target.value)}/>
          </Form.Group>
          <Form.Group className="mb-3 d-flex justify-content-center" controlId="register_button">
              <Button variant="primary" onClick={handleVerifyOTP}>Verify OTP</Button>
            </Form.Group>
        </div>
      )}
      {step === 3 && <p>Registration successful! You can now log in.</p>}
      </Card.Body>
      </Card>
    </Container> 
    </div>
   
  );
};

export default Register;
