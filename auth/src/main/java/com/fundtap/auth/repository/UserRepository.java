package com.fundtap.auth.repository;


import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

import com.fundtap.auth.modal.User;

public interface UserRepository extends JpaRepository<User, Integer> {

  Optional<User> findByEmail(String email);

}
    
